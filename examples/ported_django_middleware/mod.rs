pub mod middleware_security;
pub mod middleware_clickjacking;

use tween::stubs::{Response, Headers};

// django.http
#[allow(non_snake_case)]
pub fn HttpResponsePermanentRedirect(_: String) -> Response {
    Response {
        headers: Headers,
        body: "",
    }
}
