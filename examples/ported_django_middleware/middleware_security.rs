//! Clone of django.middleware.security.SecurityMiddleware

use tween::{Tween, TweenData, InwardAction};
use tween::stubs::{Request, Response};
use super::HttpResponsePermanentRedirect;
use tween::stubs::headers::{STRICT_TRANSPORT_SECURITY, StrictTransportSecurity,
                            X_CONTENT_TYPE_OPTIONS, XContentTypeOptions,
                            X_XSS_PROTECTION, XXssProtection};
use tween::stubs::RegexSet;

// Mapping of Django settings to SecuritySettings fields:
//
// - SECURE_HSTS_SECONDS -> sts_seconds
// - SECURE_HSTS_INCLUDE_SUBDOMAINS -> sts_include_subdomains
// - SECURE_CONTENT_TYPE_NOSNIFF -> content_type_nosniff
// - SECURE_BROWSER_XSS_FILTER -> xss_filter
// - SECURE_SSL_REDIRECT -> redirect
// - SECURE_SSL_HOST -> redirect_host
// - SECURE_REDIRECT_EXEMPT -> redirect_exempt
//
// So a practical example is:
//
//     SecurityTween::new(SecuritySettings {
//         sts_seconds: 2592000,  // 30 days
//         sts_include_subdomains: true,
//         secure_redirect_exempt: RegexSet::new(&[r"^foo/.*$"]),
//         .. Default::default(),
//     })
//
// It remains to be seen whether this approach—no central configuration source—is workable for
// real apps. I think it should be, though it will require a bit of work and a smattering of
// convention.

// This becomes purely a sentinel so that other tweens can depend on SecurityTween
// being installed before them.
pub struct Security;

// The tween has a bunch of settings; here they are!
#[derive(Clone, Default)]
pub struct SecuritySettings {
    pub sts_seconds: u32,
    pub sts_include_subdomains: bool,
    pub content_type_nosniff: bool,
    pub xss_filter: bool,
    pub redirect: bool,
    pub redirect_host: Option<String>,
    pub redirect_exempt: RegexSet,
}

// And this is the actual tween.
#[derive(Default)]
pub struct SecurityTween {
    settings: SecuritySettings,
}

impl SecurityTween {
    pub fn new(settings: SecuritySettings) -> SecurityTween {
        SecurityTween {
            settings: settings,
        }
    }
}

impl TweenData for SecurityTween {
    type Data = Security;
}

impl<C> Tween<C> for SecurityTween {
    fn inward(&self, request: &Request, _: &mut C) -> InwardAction<Security> {
        let path = if request.path.starts_with("/") {
            &request.path[1..]
        } else {
            &request.path[..]
        };
        if self.settings.redirect && !request.is_secure() &&
                !self.settings.redirect_exempt.test(path) {
            let host = match self.settings.redirect_host {
                Some(ref s) => &**s,
                None => request.get_host(),
            };
            InwardAction::Respond(HttpResponsePermanentRedirect(
                format!("https://{}{}", host, request.get_full_path())))
        } else {
            InwardAction::Continue(Security)
        }
    }

    fn outward(&self, request: &Request, mut response: Response, _input: &mut C, _data: Security) -> Response {
        if self.settings.sts_seconds != 0 && request.is_secure() {
            response.headers.entry(STRICT_TRANSPORT_SECURITY).or_insert_with(|| {
                let mut sts_header = vec![
                    StrictTransportSecurity::MaxAge(self.settings.sts_seconds)];

                if self.settings.sts_include_subdomains {
                    sts_header.push(StrictTransportSecurity::IncludeSubDomains);
                }

                sts_header
            });
        }

        if self.settings.content_type_nosniff {
            response.headers.entry(X_CONTENT_TYPE_OPTIONS)
                .or_insert(XContentTypeOptions::NoSniff);
        }

        if self.settings.xss_filter {
            response.headers.entry(X_XSS_PROTECTION) .or_insert_with(||
                vec![XXssProtection::Yes, XXssProtection::ModeBlock]);
        }

        response
    }
}
