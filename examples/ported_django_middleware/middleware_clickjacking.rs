//! Clone of django.middleware.clickjacking.XFrameOptionsMiddleware.

use tween::{Tween, TweenData, InwardAction};
use tween::stubs::{Request, Response, headers};
use tween::stubs::headers::X_FRAME_OPTIONS;

// Mapping of Django settings to XFrameOptionsSettings fields:
//
// - SECURE_HSTS_SECONDS -> sts_seconds
// - SECURE_HSTS_INCLUDE_SUBDOMAINS -> sts_include_subdomains
// - SECURE_CONTENT_TYPE_NOSNIFF -> content_type_nosniff
// - SECURE_BROWSER_XSS_FILTER -> xss_filter
// - SECURE_SSL_REDIRECT -> redirect
// - SECURE_SSL_HOST -> redirect_host
// - SECURE_REDIRECT_EXEMPT -> redirect_exempt
//
// The astute reader, who is comparing it with Django’s XFrameOptionsMiddleware, will see that
// said class uses these as its attribute names when it stores the settings in itself,
// presumably to allow easy subclassing of some form. (A waste of time, I say. No one actually
// subclasses it, I say.) Such subclassing, incidentally, is fairly easy in Python, but just
// isn’t going to fly here. You’ll probably need to replace the entire middleware to customise
// it. If you were to have something depending on it, not all is lost, because of the separation
// between data and tween code: you can define a new tween using the same data. I imagine some
// tweens will be designed with that usage in mind, with a number of mutually exclusive tweens that
// use the same data. It ends up a kind of dependency injection, really, with the data as the
// interface and the tween as the implementation. Truthfully, this could be an interesting form of
// DI, though it’s fairly limited as without HKT it can only work with the data type, not the
// behaviour. And you’re not going to be able to store non-static references in any of it.

// Sadly I don’t believe it’s feasible to avoid boxing at present (we need to be able
// to write the signature). Well, we could use function pointers. Meh, much of a muchness and
// it’s hardly going to ruin performance!
pub struct XFrameOptionsSettings {
    pub header_value: Box<Fn(&Request, &Response) -> Vec<headers::XFrameOptions>>,
}

impl Default for XFrameOptionsSettings {
    fn default() -> XFrameOptionsSettings {
        XFrameOptionsSettings {
            header_value: Box::new(|_, _| vec![headers::XFrameOptions::SameOrigin]),
        }
    }
}

// In this case we do have actual data to store in the context, and it’s not a struct
// either. Sure, we could have a `pub exempt: bool` field, but that would be boring.
// I like an enum for it instead.
pub enum XFrameOptions {
    /// Set the x-frame-options header in the response.
    Include,

    /// Don’t set the x-frame-options header in the response.
    Exempt,
}

// And this is the actual tween.
#[derive(Default)]
pub struct XFrameOptionsTween {
    settings: XFrameOptionsSettings,
}

impl XFrameOptionsTween {
    pub fn new(settings: XFrameOptionsSettings) -> XFrameOptionsTween {
        XFrameOptionsTween {
            settings: settings,
        }
    }
}

impl TweenData for XFrameOptionsTween {
    type Data = XFrameOptions;
}

impl<C> Tween<C> for XFrameOptionsTween {
    fn inward(&self, _: &Request, _: &mut C) -> InwardAction<XFrameOptions> {
        InwardAction::Continue(XFrameOptions::Include)
    }

    fn outward(&self, request: &Request, mut response: Response, _: &mut C,
               data: XFrameOptions) -> Response {
        // Don’t set it if it's already in the response
        if let XFrameOptions::Include = data {
            response.headers.entry(X_FRAME_OPTIONS).or_insert_with(|| {
                (self.settings.header_value)(request, &response)
            })
        }
        response
    }
}
