#[macro_use]
extern crate tween;
use tween::stubs::{Request, Response};
use tweens::{OneTween, TwoTween, ThreeTween};

mod tweens;

// And just to show it compiles (not necessary, but meh):
pub mod ported_django_middleware;

fn main_handler(context: &mut Context) -> Response {
    println!("\n\
              I’m the main handler. I don’t do anything. Like, *nothing*.\n\
              I have a context, though, which I guess I might as well mention:\n\
              \x1b[95m{context:#?}\x1b[m\n\
              As it stands, I don’t have access to the request. I *could* have it, but it’d\n\
              be interesting to see how people coped with a system where you literally\n\
              didn’t have the request in the main handling code and *had* to write\n\
              middleware/tweens to access it. Could potentially have upsides of being able\n\
              to serialize the context and pass it off to a different node or something.\n\
              Haven’t thought about it much.", context = context);
    println!("Oh, by the way, the value of the One is \x1b[94m{:?}\x1b[m.", context.one);
    println!("I’m thinking I’ll increment it just to show I can.\n\
              Even if I only just said that I do nothing. Just to show I can.");
    context.one.inc();
    println!("There, now it’s \x1b[94m{:?}\x1b[m.", context.one);
    context.wow();
    Response::default()
}

// Reminder: these are the tweens:
// - One, standalone;
// - Two, standalone;
// - Three, depends on One and so must come after it on pain of E0277 (“the trait bound
// `ContextForThreeTween: tween::Has<tweens::one::One>` is not satisfied”).
// Main request handler: dependencies unspecified, assume everything.

tweens! {
    handler main_handler;

    one: OneTween [ContextForOneTween];
    two: TwoTween [ContextForTwoTween] = TwoTween::new(71);
    three: ThreeTween [ContextForThreeTween];
}

impl Context {
    pub fn wow(&self) {
        // Yeah, if you want to you can drop inherent methods onto the Context. Just bear in mind
        // they’ll only be there for the final context, not the intermediate ones that tweens get.
    }
}

fn main() {
    println!("Making a tween set.");
    let tweens = Tweens::default();
    println!("The tween set shall now handle a request.");
    let _response = tweens.handle(Request::default());
    println!("\nAnd there you have it, folks. We got our response and we’re done!");
}
