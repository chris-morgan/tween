use tween::{Tween, TweenData, InwardAction};
use tween::stubs::{Request, Response};

// The Two tween doesn’t do anything meaningful.

#[derive(Debug)]
pub struct Two;

#[derive(Default)]
pub struct TwoTween {
    config_value: i32,
}

impl TwoTween {
    pub fn new(config_value: i32) -> TwoTween {
        TwoTween {
            config_value: config_value,
        }
    }
}

impl TweenData for TwoTween {
    type Data = Two;
}

impl<C> Tween<C> for TwoTween {
    fn inward(&self, _request: &Request, _input: &mut C) -> InwardAction<Two> {
        println!("");
        println!("I’m the Two tween, heading inwards.\n\
                    - I was configured with the number \x1b[93m{}\x1b[m.\n\
                    - Now I shall just say “continue!”.", self.config_value);
        InwardAction::Continue(Two)
    }

    fn outward(&self, _request: &Request, response: Response, _input: &mut C,
                _data: Two) -> Response {
        println!("");
        println!("I’m the Two tween, heading outwards. I’m a noop!");
        response
    }
}
