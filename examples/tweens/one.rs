use tween::{Tween, TweenData, InwardAction};
use tween::stubs::{Request, Response};

// The One tween doesn’t do a great deal.
// It just stores a number on the way inward and looks at it again on the way outward.

#[derive(Debug)]
pub struct One(i32);

impl One {
    pub fn inc(&mut self) {
        self.0 += 1;
    }
}

#[derive(Default)]
pub struct OneTween;

impl TweenData for OneTween {
    type Data = One;
}

impl<C> Tween<C> for OneTween {
    fn inward(&self, _request: &Request, _input: &mut C) -> InwardAction<One> {
        println!("");
        println!("I’m the One tween, heading inwards.");
        let data = One(17);
        println!("- I think I’m going to pick \x1b[94m{:?}\x1b[m as my data.", data);
        println!("- I’m going to say “continue!”, storing that as my data.");
        InwardAction::Continue(data)
    }

    fn outward(&self, _request: &Request, response: Response, _input: &mut C,
                data: One) -> Response {
        println!("");
        println!("I’m the One tween, heading outwards.");
        println!("- My data is \x1b[94m{:?}\x1b[m", data);
        println!("- I could increment it again, but I think you get the idea.");
        response
    }
}
