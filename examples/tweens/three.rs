use tween::{Tween, TweenData, Has, Context, InwardAction};
use tween::stubs::{Request, Response};

use super::one::One;

// The Three tween demonstrates a dependency on a previous tween.
// It examines and modifies the value stored by the One tween.

#[derive(Debug)]
pub struct Three;

#[derive(Default)]
pub struct ThreeTween;

impl TweenData for ThreeTween {
    type Data = Three;
}

impl<C> Tween<C> for ThreeTween where C: Context + Has<One> {
    fn inward(&self, _request: &Request, input: &mut C) -> InwardAction<Three> {
        let one = input.get_mut::<One>();
        println!("");
        println!("I’m the Three tween, heading inwards. I made sure One was run first!");
        println!("- The One value is \x1b[94m{:?}\x1b[m. I’m a-gonna increment it!", *one);
        one.inc();
        println!("- That being done (it’s now \x1b[94m{:?}\x1b[m), I’m going to say “continue!”", *one);
        InwardAction::Continue(Three)
    }

    fn outward(&self, _request: &Request, response: Response, input: &mut C,
                _data: Three) -> Response {
        let one = input.get_mut::<One>();
        println!("");
        println!("I’m the Three tween, heading outwards.");
        println!("- That One value? It’s \x1b[94m{:?}\x1b[m. Boring. I’ll increment it again.",
                    *one);
        one.inc();
        println!("- That being done, it’s \x1b[94m{:?}\x1b[m and I’m letting you go on.",
                    *one);
        response
    }
}

