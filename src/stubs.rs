//! A bunch of stubs, to assist with the proof of concept.
//!
//! Some of the stuff in here is needed for the ported Django middleware and is otherwise
//! pointless. Meh.

#![allow(dead_code)]
#![allow(non_camel_case_types, non_snake_case)]

use core::marker::PhantomData;

#[derive(Default, Clone)]
pub struct RegexSet;

impl RegexSet {
    pub fn test(&self, _: &str) -> bool { false }
}

#[derive(Default)]
pub struct Headers;

impl Headers {
    pub fn entry<T, U>(&mut self, _: T) -> HeaderEntry<U> { HeaderEntry(PhantomData) }
}

pub struct HeaderEntry<U>(PhantomData<U>);
impl<U> HeaderEntry<U> {
    pub fn or_insert(self, _: U) { }
    pub fn or_insert_with<F: FnOnce() -> U>(self, _: F) { }
}

#[derive(Default)]
pub struct Request {
    pub headers: Headers,
    pub path: &'static str,
}

impl Request {
    pub fn is_secure(&self) -> bool { false }
    pub fn get_host(&self) -> &str { "" }
    pub fn get_full_path(&self) -> &str { "" }
}

fn noop() { }

pub static mut RESPONSE_DROP_CALLBACK: fn() = noop;

#[derive(Default)]
pub struct Response {
    pub headers: Headers,
    pub body: &'static str,
}

impl Drop for Response {
    fn drop(&mut self) {
        unsafe {
            (RESPONSE_DROP_CALLBACK)();
        }
    }
}

pub mod headers {
    pub struct X_FRAME_OPTIONS;
    pub enum XFrameOptions {
        SameOrigin,
    }

    pub struct STRICT_TRANSPORT_SECURITY;
    pub enum StrictTransportSecurity {
        MaxAge(u32),
        IncludeSubDomains,
    }

    pub struct X_CONTENT_TYPE_OPTIONS;
    pub enum XContentTypeOptions {
        NoSniff,
    }

    pub struct X_XSS_PROTECTION;
    pub enum XXssProtection {  // 1; mode=block
        Yes,
        ModeBlock,
    }
}
