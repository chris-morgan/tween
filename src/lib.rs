#![no_std]

extern crate nodrop;

pub mod stubs;

/// Implementation details of the macro, only public so the macro sees it. Not for direct use.
#[doc(hidden)]
pub mod __ {
    pub use core::mem::{transmute, forget, replace, uninitialized};
    pub use core::ptr::copy_nonoverlapping;
    pub use nodrop::NoDrop;
    use core::ptr;
    use core::ops::{Deref, DerefMut};

    /// A `&mut` reference which will, when dropped, call drop on what it refers to. Obviously
    /// wildly unsafe. Use with caution, &c.
    pub struct DropMutRef<'a, T: 'a>(&'a mut T);

    impl<'a, T: 'a> DropMutRef<'a, T> {
        pub unsafe fn new(inner: &'a mut T) -> DropMutRef<'a, T> {
            DropMutRef(inner)
        }
    }

    impl<'a, T: 'a> Drop for DropMutRef<'a, T> {
        fn drop(&mut self) {
            // It’d be nice to just do `self.0.drop();`, but that’s practically always stupid, and
            // Rust protects you from doing that. I just happen to be doing it for a good reason
            // here. Not that that is a sufficient defence against an accusation of stupidity…
            unsafe {
                ptr::read(&**self);
            }
        }
    }

    impl<'a, T: 'a> Deref for DropMutRef<'a, T> {
        type Target = T;

        fn deref(&self) -> &T {
            &*self.0
        }
    }

    impl<'a, T: 'a> DerefMut for DropMutRef<'a, T> {
        fn deref_mut(&mut self) -> &mut T {
            self.0
        }
    }

}

use stubs::{Request, Response};

pub enum InwardAction<T> {
    Continue(T),
    Respond(Response),
    Abort,
}

pub trait TweenData {
    type Data;
}

pub trait Tween<InputContext>: TweenData {
    fn inward(&self, request: &Request, context: &mut InputContext) -> InwardAction<Self::Data>;

    #[allow(unused_variables)]
    fn outward(&self, request: &Request, response: Response, context: &mut InputContext,
               data: Self::Data) -> Response {
        response
    }
}

/* Ideally we’ll be able to do something like this, so that normally inward and outward can both
 * be omitted. Even if omitting both probably doesn’t make a great deal of sense.
partial impl<ContextImpl> Tween<InputContext> for T where T::Data: Default {
    fn inward(&self, request: &Request, context: &mut InputContext) -> InwardAction<Self::Data> {
        Continue(Default::default())
    }
}
*/

pub trait Has<T>: AsRef<T> + AsMut<T> { }
impl<C, T> Has<T> for C where C: Context, C: AsRef<T> + AsMut<T> { }

pub trait Context: Sized {
    type Tween: Tween<Self>;

    #[inline]
    fn get<T>(&self) -> &T where Self: Has<T> {
        self.as_ref()
    }

    #[inline]
    fn get_mut<T>(&mut self) -> &mut T where Self: Has<T> {
        self.as_mut()
    }
}

// Dummy struct with dummy implementation; we could probably come up with a way of ditching it if
// we tried, but it would take effort and I don’t see any advantage at present.
#[derive(Default)]
pub struct AllTweens;
impl TweenData for AllTweens {
    type Data = ();
}
impl<C> Tween<C> for AllTweens {
    fn inward(&self, _request: &Request, _input: &mut C) -> InwardAction<()> {
        InwardAction::Continue(())
    }
}

/// Make a nicely executable tweens chain.
///
/// This inserts two public types into the scope it is implemented in:
///
/// - `Tweens`, a struct for storing all the tween instances and handling a request
/// - `Context`, the context for the main handler, with all the fields for it.
///
/// Sample usage:
///
/// ```rust,ignore
/// tweens! {
///     handler |context: Context| {
///         // ... something to produce a Response ...
///     };
/// 
///     one: OneTween [ContextForOneTween];
///     two: TwoTween [ContextForTwoTween] = TwoTween::new(71);
///     three: ThreeTween [ContextForThreeTween];
/// }
/// ```
///
/// Some things to note:
///
/// - The keyword `handler` is followed by an expression which is callable, taking a `Context` and
///   returning a `Response`.
///
/// - `ContextForOneTween` et al. are just arbitrary unique identifiers that are used for private
///   types. I want to be able to remove that part altogether, generating names for them like
///   `__Context_one`, but to do that we need [eager macro expansion][], so no go for now.
///   Hopefully this will land and become stable by the time Weft is trying to support stable.
///
///   (Another approach would be to make a `__contexts` module containing the contexts, reusing the
///   field names again. This would require the type names to be absolute paths lest `OneTween` et
///   al. not be in scope in the new `__contexts` module.)
///
///   (Yet another approach would be to use tuples for the anonymous context structs. I’m almost
///   certain this approach could be made to work, but I feel it’s ugly, and I’d want to change it
///   back to structs once eager macro expansion is a thing, so I’m just going to skip it. It could
///   have performance issues too if `let (ref one, ref two, ref three) = context; one` is
///   going to perform worse than `context.0`, though I expect it optimises those unused
///   references out of existence. Actually, this won’t work properly because the tuples would be
///   `repr(Rust)`, while the contexts need to be `repr(C)` to ensure compatibility between them
///   all.)
///
/// - The tweens are executed in order: one inwards, then two inwards, then three inwards, then the
///   main handler, then three outwards, two outwards and one outwards.
///
/// - A tween will be constructed with `Default::default()` by default; you can override that with
///   `= expr` as shown above with `= TwoTween::new(71)`.
///
/// [eager macro expansion]: https://github.com/rust-lang/rfcs/pull/1628
#[macro_export]
macro_rules! tweens {
    // The things in square brackets are things I’d like to remove the need of.
    (
        handler $main_handler:expr;
        $($field:ident: $ty:ty [$context:ident]$(= $init:expr)*;)*
    ) => {
        pub struct Tweens {
            $($field: $ty,)*
        }

        impl Default for Tweens {
            fn default() -> Tweens {
                Tweens {
                    $($field: tweens!(@init, $($init)*),)*
                }
            }
        }

        impl Tweens {
            pub fn handle(&self, request: $crate::stubs::Request) -> $crate::stubs::Response {
                use $crate::Tween;
                // We wrap the main handler up in this function so that it doesn’t sit inside an
                // unsafe block, which leads to unexpected behaviour. ($main_handler might be the
                // name of a function, or it might be a closure, which would then be in unsafe.)
                #[inline(always)]
                fn __main_handler(context: &mut Context) -> $crate::stubs::Response {
                    $main_handler(context)
                }
                // Yep, it’s a honkin’ big unsafe block. Performance, baby!
                unsafe {
                    let mut context =
                        $crate::__::NoDrop::<Context>::new($crate::__::uninitialized());

                    tweens! {
                        @exec_all, self, context, request, __main_handler,
                        $(($field, $context),)*
                    }

                    // All the destructors on context items that needed to be run have been run by
                    // their values being passed to the appropriate tween’s outward method call.
                    // The NoDrop wrapping on context stops it from running the destructors a
                    // second time, so all’s good.

                    // Note that if we’re unwinding because of a panic, anything in the context
                    // will be leaked. But it’s all memory-safe, so that’s good.
                }
            }
        }

        tweens! {
            @define_context
            [$($field: $ty [$context];)*],
            []
        }
    };

    (@init, $init:expr) => {
        $init
    };

    (@init, ) => {
        Default::default()
    };

    (@exec, $self_:ident, $context_:ident, $request_:ident,
     $field:ident, $context:ident, $next:expr) => {{
        let mut context = $crate::__::DropMutRef::<$context>::new(
            $crate::__::transmute(&mut *$context_));
        let response = match $self_.$field.inward(&$request_, &mut *context) {
            $crate::InwardAction::Continue(data) => {
                $crate::__::forget($crate::__::replace(&mut $context_.$field, data));
                // Forget this context so it doesn’t get dropped on panic; $next will make a new
                // DropMutRef of a larger subcontext, so it’ll all be dropped appropriately.
                $crate::__::forget(context);
                let response = $next;
                // But now we’ll need the context subset again, for the outward call.
                // This will ensure the right stuff gets dropped in case of panic in outward.
                context = $crate::__::DropMutRef::new($crate::__::transmute(&mut *$context_));
                let mut data = $crate::__::uninitialized();
                $crate::__::copy_nonoverlapping(&$context_.$field, &mut data, 1);
                // $context_.$field is now effectively uninitialized.
                $self_.$field.outward(&$request_, response, &mut *context, data)
            },
            $crate::InwardAction::Respond(response) => {
                response
            },
            $crate::InwardAction::Abort => unimplemented!(),
        };
        // We don’t want to drop the context now. Continue orderly unraveling.
        $crate::__::forget(context);
        response
    }};

    (@exec_all, $self_:ident, $context_:ident, $request_:ident, $main_handler:expr,
     ($field:ident, $context:ident),) => {{
        tweens! {
            @exec, $self_, $context_, $request_,
            $field, $context, {
                // If the main handler panics and we unwind, we want the whole context to be
                // dropped. This prevents us from leaking it all.
                let mut context = $crate::__::DropMutRef::new(&mut *$context_);
                let response = $main_handler(&mut *context);
                // Don’t want to drop the whole context now. We want to unravel it piece by piece.
                $crate::__::forget(context);
                response
            }
        }
    }};

    (@exec_all, $self_:ident, $context_:ident, $request_:ident,
     $main_handler:expr,
     ($field:ident, $context:ident),
     $($remainder:tt)+) => {
        tweens! {
            @exec, $self_, $context_, $request_,
            $field, $context,
            tweens! {
                @exec_all, $self_, $context_, $request_, $main_handler,
                $($remainder)+
            }
        }
    };

    (@define_context [
        $field:ident: $ty:ty [$context:ident];
        $($rest_field:ident: $rest_ty:ty [$rest_context:ident];)*
    ], [
        $($prev_field:ident: $prev_ty:ty [$prev_context:ident];)*
    ]) => {
        #[derive(Debug)]
        #[repr(C)]
        struct $context {
            $($prev_field: <$prev_ty as $crate::TweenData>::Data,)*
        }

        tweens!(@impl_context_type $context for $ty {
            $($prev_field: <$prev_ty as $crate::TweenData>::Data,)*
        });

        tweens!(
            @define_context
            [
                $($rest_field: $rest_ty [$rest_context];)*
            ],
            [
                $($prev_field: $prev_ty [$prev_context];)*
                $field: $ty [$context];
            ]
        );
    };

    (@define_context [], [
        $($field:ident: $ty:ty [$context:ident];)*
    ]) => {
        // Difference from the rest of the contexts: pub struct with pub fields.
        // (The others don’t need pub fields as they’re only ever accessed via a generic <C:
        // Context + Has<...>>, so there’s never any field access at all.)
        #[derive(Debug)]
        #[repr(C)]
        pub struct Context {
            $(pub $field: <$ty as $crate::TweenData>::Data,)*
        }

        tweens!(@impl_context_type Context for $crate::AllTweens {
            $($field: <$ty as $crate::TweenData>::Data,)*
        });
    };

    (@impl_context_type $context:ident for $tween:ty {
        $($data_ident:ident: $data_type:ty,)*
    }) => {
        // This implementation provides tween dependency type checking.
        impl $crate::Context for $context {
            type Tween = $tween;
        }

        $(impl AsRef<$data_type> for $context {
            #[inline]
            fn as_ref(&self) -> &$data_type {
                &self.$data_ident
            }
        }

        impl AsMut<$data_type> for $context {
            #[inline]
            fn as_mut(&mut self) -> &mut $data_type {
                &mut self.$data_ident
            }
        })*
    };
}
