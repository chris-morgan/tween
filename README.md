Tween
=====

**What is this?** This is a proof-of-concept middleware library for web
frameworks. (“Tween” is a synonym of middleware, the bits that come between the
underlying HTTP server and the router’s main request handling function. Why did
I call it tween rather than middleware? Mostly a certain fondness for the
Pyramid web framework for Python which calls ’em that.)

**Key features:** robustness (no cause for panicking) and performance (no need
for heap memory), and static tween dependency declaration.

**Staus:** WIP, POC, TLA. Repository history is liable to be thrown away at the
drop of a hat. Do not depend on it in any way, shape, form or otherwise. Just
to be sure of that, I haven’t published any license, so you can’t use it
anyway! (In other words, it’s *totally* not because I’m lazy—it’s a feature
instead!) This repository is all about ideas at present.

Things to try
-------------

Requires Rust 1.10+ (Rust 1.9.0, stable at the time of writing, hits an
internal compiler error).

A couple of things you might want to try:

- **`cargo test`**: run all the tests (makes sure the tweens get executed in the
  right place, memory safety is maintained, &c.).

- **`cargo run --example simple`**: demonstrates a few simple tweens and how it
  all works. (It also compiles the sample ported Django middlewares, but does
  nothing with them.)

Features
--------

This repository is all about ideas at present.

**Key features:** robustness and performance.

Existing schemes haven’t solved the dependency problem (B depends on A, so A
must be earlier in the middleware chain), and they tend to use complex
trait-object-heavy (and thus allocation-heavy) approaches to storing data.

I decided to solve both of these together.

- Each tween stores a value into a context object, which can be accessed by
  later tweens (if they declare that they depend on the earlier tween) and the
  main handler.

- Everything is checked at compile time. Nothing in the tween library/macro
  code can panic. No `Option` unwrapping that might panic if misconfigured or
  anything like that.

- Performance should be optimal, as the abstractions all boil down to simple
  field access.

- There is a main request handling function. Sure, the entire site could be
  done as tweens, but I think it’ll probably be nicer not done that way.
  This design decision is trivial to change.

- I’m experimenting with the concept of the main request handler *not having
  access to the request object at all*: anything from the request that you
  wanted, a tween would need to retrieve and store as its data. Justification
  of this idea below. It still has some downsides, so I’m not sure it’ll stay.

- You get to put one value per tween into the context object. This is
  effectively namespacing such things rather than making it flat (or
  anarchical!) as it normally is. Maybe good, maybe bad. Not what people are
  used to, anyway.

- Accessing the data produced by tweens: `context.foo` in the main handler,
  `context.get::<Foo>()` in later tweens that depend on the earlier tweens (not
  `context.foo` because tweens basically get a context of an anonymous type and
  the field names that application code gets to use are chosen by the
  application code, on which tweens are not dependent).

Technical explanation
---------------------

You may look at the code and see a hideous number of memory-unsafe things
happening. It’s true: this library’s foundation is *very* unsafe stuff.
But the interface it exposes is all nice and safe.

Each tween gets to store exactly one object in the context. We’ll assume for
here that our tweens are called `ATween` (for some value of A) and their data
`AData` (for the same value of A as its tween).

We’ll use this as our example definition:

```rust
tweens! {
    handler main_handler;  // main_handler being a fn(&mut Context) -> Response

    a: ATween [ContextForA];
    b: BTween [ContextForB];
    c: CTween [ContextForC];
}
```

The context of the main handler is this:

```rust
pub struct Context {
    pub a: AData,
    pub b: BData,
    pub c: CData,
}
```

This looks like this in memory:

    ┏━━━━━━━┳━━━━━━━┳━━━━━━━┓
    ┃ AData ┃ BData ┃ CData ┃
    ┃  :-)  ┃  :-)  ┃  :-)  ┃
    ┗━━━━━━━┻━━━━━━━┻━━━━━━━┛

Each tween in its ingoing and outgoing functions gets a subset of this context,
the context that has been constructed thus far, and produces the next value. So
`ATween` gets an empty context and produces an `AData`, while `CData` takes a
context with the `AData` and the `BData`, producing a `CData`.

Basically, we create new context types with dummy names (which currently have
to be specified by the user, can’t use `concat_idents!` on it, alas, due to
non-eager macro expansion), with the values that have been initialised thus
far, and transmute a reference to the complete context to a reference to the
subcontext type.

So the `ATween` gets something that looks like this, with the boldly boxed area
being available to it and the thinly boxed area not yet constructed:

```rust
struct ContextForA {
}
```

    ┎───────┬───────┬───────┐
    ┃ AData │ BData │ CData │
    ┃  ---  │  ---  │  ---  │
    ┖───────┴───────┴───────┘

`ATween` constructs its `AData` (presuming it doesn’t stop processing, sending
a response down the chain instead) and processing continues. `BTween` gets a
context with an `AData` in it (incidentally, note also that the struct and its
fields are private, unlike the main context; they’re an implementation detail):

```rust
struct ContextForB {
    a: AData,
}
```

    ┏━━━━━━━┱───────┬───────┐
    ┃ AData ┃ BData │ CData │
    ┃  :-)  ┃  ---  │  ---  │
    ┗━━━━━━━┹───────┴───────┘

You know, I should really make some nice SVG diagrams of this. Ah well. I’m
having too much fun using the Box Drawing Unicode block. I always do. (Then
again, I enjoy hand-writing SVG as well.)
Vim users: you can use much of the block nice and easily with digraphs, like
<kbd>Ctrl+K D r</kbd> producing `┎` (heavy down, light right), and <kbd>Ctrl+K
h d</kbd> producing `┬` (horizontal, down). You can figure it out if you’re
interested. I do like my digraphs, just like I like my Compose key so I can
type my curly quotation marks in in whatever tool without any bother, just
<kbd>Compose + ' + '</kbd> = `’`. it becomes completely automatic, I type in my
quotation marks curly, my dashes emmy and my ellipses Unicodey—it’s the way I
work…

But enough digression.

`CTween` gets a context with `AData` and `BData` in it, and produces a `CData`:

```rust
struct ContextForC {
    a: AData,
    b: BData,
}
```

    ┏━━━━━━━┳━━━━━━━┱───────┐
    ┃ AData ┃ BData ┃ CData │
    ┃  :-)  ┃  :-)  ┃  ---  │
    ┗━━━━━━━┻━━━━━━━┹───────┘

Finally, the main handler gets called with the completed context. For
convenience, it gets a struct named `Context`, so you can access fields on it
(like `context.a`) rather than needing to use method calls like
`context.get::<AData>()`. You could implement inherent methods on this
`Context` as well; it’s defined by the `tweens!` macro as a public type in the
same scope.

```rust
pub struct Context {
    pub a: AData,
    pub b: BData,
    pub c: CData,
}
```

    ┏━━━━━━━┳━━━━━━━┳━━━━━━━┓
    ┃ AData ┃ BData ┃ CData ┃
    ┃  :-)  ┃  :-)  ┃  :-)  ┃
    ┗━━━━━━━┻━━━━━━━┻━━━━━━━┛

On the way out, `CTween` gets the two-element context, with the `CData` copied
so it can be passed by value.

Then `BTween` gets a one-element context and its `BData`.
Then `ATween` its zero- and its `AData`.

`CTween` can declare that it needs a context containing an `AData`, for
example, and so tween dependency is made to work. (It’d access it as
`context.get::<AData>()`, because its context type is generic, so no field
access. Plus the actual field names are determined by application code, not
tween library code.) If you try to put a `CTween` before an `ATween` (or if
there *is* no `ATween`) you’ll get a compilation error. This is good.

Through much trickery and subterfuge, destructors are all run when appropriate,
even in case of unwinding due to panicking. Feast your eyes on the code if you
want to look into the messy details, or observe that the tests, which verify
destructor runs to be appropriate, pass. You can just trust the system if you
like. Each member of the context will be freed exactly once, with destructors
never running on uninitialised components, whether a panic occurs inside a
tween on the way in, or in the main handler, or inside a tween on the way out.

Dependency directions
---------------------

As has been demonstrated thus far, tweens can depend on other tweens (or
rather, data produced by other tweens) being *ealier* in the chain. It would be
quite possible to add another trait like `Has` for afterwards-dependencies.
When considering the classical onion view of middleware, this is a tween
knowing about what’s deeper in the onion, where at present it only knows what
layers have already been peeled away. I can’t think of any compelling use case
for this feature, though, so it probably won’t go in. Mostly, there’s no clear
way for inner tweens to pass data to outer ones, even on the way outwards: the
data for the inner middleware has already been dropped. (You could potentially
store something inside the response object, but that seems likely to be a bad
code smell.) Such a feature could be used to guarantee that all or none of a
set of tweens are installed, but I don’t think that’s all that useful either.
If anyone can come up with a good reason to produce this trait, it can be
implemented. If not, let it lie.

I suspect trait negation is a more interesting concept to apply.

(An idea of how it might be useful: struct F indicates that a given feature is
available. A given implementation of it may wish to have other data as well,
though, but only one piece of data per tween, so you’d need two tweens to
represent the concept, each of which could depend on the existence of the
other’s data. This would be weak certainty, of course, as something else could
be implementing `F`, and the case doesn’t feel very compelling either. It’s
just an idea that occurred to me—maybe it’ll help you to think of other things
that might use this and actually be useful.)

Idea: no access to the actual request in the main request handler
-----------------------------------------------------------------

It’s not uncommon for one to wish to override parts of the HTTP message. For
example, allowing the `X-Http-Method-Override` header to override the method,
or the `X-Forwarded-Host` header to overriding the `Host` header. The standard
solution in Python web frameworks ends up being things like `request.host`
being different from `request['host']` (written `request.headers['host']` in
some frameworks) as it is in some. It winds up messy, quite frankly, with some
things being accessed as headers and others as attributes directly on the
request object. So why not just do it all as a context object, with the tweens
having access to the request, but the actual handler code *not* having access
to the request?

I think it’s workable. Might play around with it. Could have advantages of
being able to divorce HTTP/TCP/IP stuff from request processing; uncertain.

Similar could possibly occur for responses. Uncertain.

How the `tweens!` macro expands
-------------------------------

It’s all managed by a fairly opaque bit of macro (`tweens!`),
but I can explain in somewhat more detail how it all works here.

As before, this is the basic macro invocation that we’ll be considering.

```rust
tweens! {
    handler main_handler;  // main_handler being a fn(&mut Context) -> Response

    a: ATween [ContextForA];
    b: BTween [ContextForB];
    c: CTween [ContextForC];
}
```

`ATween`, `BTween` and `CTween` implement `Tween` with respective `Data` types
`AData`, `BData` and `CData`. (For convenience, in the examples below I’ll be
writing `AData` where the macro actually writes `<ATween as Tween>::Data`.)

Note that I don’t show precisely what `tweens!` actually expands to in this
section; that’s just a bit too scary and unsafe. I show a memory-safe version
of it, which makes it all clearer and more straightforward. If thou wouldst see
unfettered the horror of the macro, look thou into `lib.rs`—behold, and
beholding fall silent in dread. Look, mortal! See what unsafe code abounds!
What tortured monstrosities! Mutable references that drop their referent;
destructors destined for doom; mutable aliasing! Reel and stagger! Clutch thy
hands to thine eyes and moan softly for the woes come upon thee! Abandon all
hope, ye who enter here.

Or if you prefer, you could just use the `tweens!` macro without worrying about
what’s in it. You’ll probably be happier.

### Doing it the memory-safe way

At each stage, you have an instance of the tween’s context. Pass a mutable
reference to it to the tween’s `inwards` method and you’ll get an instance of
that tween’s data; then, construct an instance of the next context type
containing all of the fields of the current context plus the new piece of data
from the tween.

Here’s a simplified example of what it looks like with the `tweens!` macro expanded.

The simplifications done are:

- Considering `inwards` to be unable to return a response early,
  short-circuiting the rest of the tweens and the main handler; this makes the
  code easier to follow; a version without that shortcut follows.

- Skipped the details of some implementations of things (and `Default` isn’t
  actually derived, though the default is equivalent to deriving `Default`)

- Contexts are done in a memory-safe way (that complicates the code more, and
  demonstrating an obviously memory-safe version is the intention here)

```rust
struct ContextForA {
}

// impl tween::Context for ContextForA

struct ContextForB {
    a: AData,
}

// impl AsRef<AData>, AsMut<AData> for ContextForB
// impl tween::Context for ContextForB

struct ContextForC {
    a: AData,
    b: BData,
}

// impl AsRef<AData>, AsMut<AData> for ContextForC
// impl AsRef<BData>, AsMut<BData> for ContextForC
// impl tween::Context for ContextForC

struct Context {
    a: AData,
    b: BData,
    c: CData,
}

// impl AsRef<AData>, AsMut<AData> for Context
// impl AsRef<BData>, AsMut<BData> for Context
// impl AsRef<CData>, AsMut<CData> for Context
// impl tween::Context for Context

#[derive(Default)]
struct Tweens {
    a: ATween,
    b: BTween,
    c: CTween,
}

impl Tweens {
    pub fn handle(&self, request: Request) -> Response {
        // Inwards: the A tween, then the B, then the C.
        let mut a_context = ContextForA { };
        let a_data = self.a.inward(&request, &mut a_context);

        let mut b_context = ContextForB {
            a: a_data,
        };
        let b_data = self.b.inward(&request, &mut b_context);

        let mut c_context = ContextForC {
            a: b_context.a,
            b: b_data,
        };
        let c_data = self.c.inward(&request, &mut c_context);

        // Now the main handler
        let mut context = Context {
            a: c_context.a,
            b: c_context.b,
            c: c_data,
        };
        let mut response = main_handler(context);

        // Now outwards again: C, then B, then A.
        let mut c_context = ContextForC {
            a: context.a,
            b: context.b,
        };
        response = self.c.outward(&request, response, &mut c_context, context.c);

        let mut b_context = ContextForB {
            a: c_context.a,
        };
        response = self.b.outward(&request, response, &mut b_context, c_context.b);

        let mut a_context = ContextForA { };
        response = self.a.outward(&request, response, &mut a_context, b_context.a);

        // Now we’re all done!
        response
    }
}
```

Here’s a more complete version of `Tweens.handle`, supporting returning a response early, skipping the rest of the tweens and the main handler:

```rust
impl Tweens {
    pub fn handle(&self, request: Request) -> Response {
        let mut a_context = ContextForA { };
        let (response, a_context) = match self.a.inward(&request, &mut a_context) {
            InwardAction::Continue(a_data) => {
                let mut b_context = ContextForB {
                    a: a_data,
                };

                let (response, b_context) = match self.b.inward(&request, &mut b_context) {
                    InwardAction::Continue(b_data) => {
                        let mut c_context = ContextForC {
                            a: b_context.a,
                            b: b_data,
                        };

                        let mut response = match self.c.inward(&request, &mut c_context) {
                            InwardAction::Continue(c_data) => {
                                let mut context = Context {
                                    a: c_context.a,
                                    b: c_context.b,
                                    c: c_data,
                                };

                                let mut response = main_handler(&mut context);

                                let mut c_context = ContextForC {
                                    a: context.a,
                                    b: context.b,
                                };
                                response = self.c.outward(&request, response, &mut c_context, context.c);
                                (response, c_context)
                            },
                            InwardAction::Respond(response) => (response, c_context),
                        };

                        let mut b_context = ContextForB {
                            a: c_context.a,
                        };
                        response = self.b.outward(&request, response, &mut b_context, c_context.b);
                        (response, b_context)
                    },
                    InwardAction::Respond(response) => (response, b_context),
                };

                let mut a_context = ContextForA { };
                let response = self.a.outward(&request, response, &mut a_context, b_context.a);
                (response, a_context)
            },
            InwardAction::Respond(response) => (response, a_context),
        };
        response
    }
}
```

### Improving performance at the cost of memory safety

Taking the example from before, you’ll notice that `a_context`, `b_context` and
`c_context` are all prefixes of `context`. So why allocate new places for the
contexts each time when you could instead just store `context`, and pretend
that it’s a `ContextForA`, then a `ContextForB`, then a `ContextForC`? Yeah,
it’s quite plausible to do it this way, and for performance one should. (It’s
also in various ways easier to write the macros, but let *that* pass.)

Of course, you then have the problem that your `Context` object is, before the
main handler, only partially initialised; in case of panic, you may get
destructors running on uninitialised memory, which is probably fatal. So you’ve
got to handle that. Well, that’s what `NoDrop` and `DropMutRef` in the code are
all about.

Again, after the main handler, each field of the context when you copy it out
while leaving the existing value there becomes a ticking time bomb sitting in
your hands which will explode as soon as you drop it.

Fortunately, there’s a solution. Just forget about the bomb.

OK, so the analogy falls apart once we misapply our jargon. In the interests of
public safety, I should warn you that despite my advice to forget a bomb in
order to defuse it, and the saying “out of sight, out of mind,” forgetting
about a ticking time bomb in real life is unlikely to defuse it. Unless it’s
some crazy psycho-something-or-other bomb which is reading your mind. Whoa. Now
*that’s* an interesting thought.

OK, so I wrote this entire section just for that joke. It was fun to write.
