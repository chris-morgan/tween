#[macro_use]
extern crate tween;

#[macro_use]
extern crate lazy_static;

use std::sync::{RwLock, Mutex, MutexGuard};

use tween::{Tween, TweenData, InwardAction};
use tween::stubs::{Request, Response, Headers};

lazy_static! {
    pub static ref ACTIONS: RwLock<Vec<(&'static str, &'static str)>> = RwLock::new(vec![]);
    // Tests using the log must not run in parallel, because it’s global.
    // They must lock this before doing anything that touches the log.
    pub static ref LOG_LOCK: Mutex<()> = Mutex::new(());
}

fn log_response_drop() {
    log("Response", "drop");
}

fn lock<'a>() -> MutexGuard<'a, ()> {
    unsafe {
        tween::stubs::RESPONSE_DROP_CALLBACK = log_response_drop;
    }
    match LOG_LOCK.lock() {
        Ok(guard) => guard,
        Err(poison) => poison.into_inner(),
    }
}

fn log(ty: &'static str, meth: &'static str) {
    ACTIONS.write().unwrap().push((ty, meth));
}

fn get_actions() -> Vec<(&'static str, &'static str)> {
    let out = ACTIONS.read().unwrap().clone();
    ACTIONS.write().unwrap().clear();
    out
}

pub enum DesiredAction {
    Continue,
    Respond,
    PanicInward,
    PanicOutward,
}

impl Default for DesiredAction {
    fn default() -> DesiredAction {
        DesiredAction::Continue
    }
}

fn response(body: &'static str) -> Response {
    Response {
        headers: Headers,
        body: body,
    }
}

macro_rules! make_tween {
    ($data:ident, $tween:ident) => {

        #[derive(Debug)]
        pub struct $data;

        impl $data {
            fn new() -> $data {
                log(stringify!($data), "new");
                $data
            }
        }

        impl Drop for $data {
            fn drop(&mut self) {
                log(stringify!($data), "drop");
            }
        }

        #[derive(Default)]
        pub struct $tween {
            desired_action: DesiredAction,
        }

        impl $tween {
            pub fn new(desired_action: DesiredAction) -> $tween {
                $tween {
                    desired_action: desired_action,
                }
            }
        }

        impl TweenData for $tween {
            type Data = $data;
        }

        impl<C> Tween<C> for $tween {
            fn inward(&self, _request: &Request, _input: &mut C) -> InwardAction<$data> {
                log(stringify!($tween), "inward");
                match self.desired_action {
                    DesiredAction::Continue | DesiredAction::PanicOutward =>
                        InwardAction::Continue($data::new()),
                    DesiredAction::Respond => InwardAction::Respond(response(stringify!($tween))),
                    DesiredAction::PanicInward => {
                        log(stringify!($tween), "inward panic");
                        panic!("I was told to panic on the way in, so I did.")
                    },
                }
            }

            fn outward(&self, _request: &Request, response: Response, _input: &mut C,
                        _data: $data) -> Response {
                log(stringify!($tween), "outward");
                if let DesiredAction::PanicOutward = self.desired_action {
                    log(stringify!($tween), "outward panic");
                    panic!("I was told to panic on the way out, so I did.")
                }
                response
            }
        }

    }
}

make_tween!(One, OneTween);
make_tween!(Two, TwoTween);
make_tween!(Three, ThreeTween);
make_tween!(Four, FourTween);
make_tween!(Five, FiveTween);

mod normal_flow {
    use super::{OneTween, TwoTween, ThreeTween, FourTween, FiveTween};
    use tween::stubs::{Request};

    tweens! {
        handler |_context: &mut Context| {
            super::log("main", "handler");
            super::response("main")
        };

        one: OneTween [ContextForOneTween];
        two: TwoTween [ContextForTwoTween];
        three: ThreeTween [ContextForThreeTween];
        four: FourTween [ContextForFourTween];
        five: FiveTween [ContextForFiveTween];
    }

    #[test]
    fn execute() {
        let tweens = Tweens::default();
        let _lock = super::lock();
        let response = tweens.handle(Request::default());
        assert_eq!(response.body, "main");
        drop(response);
        assert_eq!(super::get_actions(), [
            ("OneTween", "inward"),
            ("One", "new"),
            ("TwoTween", "inward"),
            ("Two", "new"),
            ("ThreeTween", "inward"),
            ("Three", "new"),
            ("FourTween", "inward"),
            ("Four", "new"),
            ("FiveTween", "inward"),
            ("Five", "new"),
            ("main", "handler"),
            ("FiveTween", "outward"),
            ("Five", "drop"),
            ("FourTween", "outward"),
            ("Four", "drop"),
            ("ThreeTween", "outward"),
            ("Three", "drop"),
            ("TwoTween", "outward"),
            ("Two", "drop"),
            ("OneTween", "outward"),
            ("One", "drop"),
            ("Response", "drop"),
        ]);
    }
}

mod early_return {
    use super::{OneTween, TwoTween, ThreeTween, FourTween, FiveTween};
    use tween::stubs::{Request};

    tweens! {
        handler |_context: &mut Context| {
            super::log("main", "handler");
            super::response("main")
        };

        one: OneTween [ContextForOneTween];
        two: TwoTween [ContextForTwoTween];
        three: ThreeTween [ContextForThreeTween] = ThreeTween::new(::DesiredAction::Respond);
        four: FourTween [ContextForFourTween];
        five: FiveTween [ContextForFiveTween];
    }

    #[test]
    fn execute() {
        let tweens = Tweens::default();
        let _lock = super::lock();
        let response = tweens.handle(Request::default());
        assert_eq!(response.body, "ThreeTween");
        drop(response);
        assert_eq!(super::get_actions(), [
            ("OneTween", "inward"),
            ("One", "new"),
            ("TwoTween", "inward"),
            ("Two", "new"),
            ("ThreeTween", "inward"),
            ("TwoTween", "outward"),
            ("Two", "drop"),
            ("OneTween", "outward"),
            ("One", "drop"),
            ("Response", "drop"),
        ]);
    }
}

mod handler_panic {
    use super::{OneTween, TwoTween, ThreeTween, FourTween, FiveTween, log};
    use tween::stubs::{Request};
    use std::panic;

    tweens! {
        handler |_context: &mut Context| {
            log("main", "panic");
            panic!("The handler is upset and will now panic.");
        };

        one: OneTween [ContextForOneTween];
        two: TwoTween [ContextForTwoTween];
        three: ThreeTween [ContextForThreeTween];
        four: FourTween [ContextForFourTween];
        five: FiveTween [ContextForFiveTween];
    }

    #[test]
    fn execute() {
        let tweens = Tweens::default();
        let _lock = super::lock();
        let response = panic::catch_unwind(|| {
            tweens.handle(Request::default())
        });
        assert!(response.is_err());
        assert_eq!(super::get_actions(), [
            ("OneTween", "inward"),
            ("One", "new"),
            ("TwoTween", "inward"),
            ("Two", "new"),
            ("ThreeTween", "inward"),
            ("Three", "new"),
            ("FourTween", "inward"),
            ("Four", "new"),
            ("FiveTween", "inward"),
            ("Five", "new"),
            ("main", "panic"),
            // Note that they are dropped in forward order, not backward order. This is OK, because
            // the type signatures make sure that tween datas cannot contain references to each
            // other. (Now *that* would be a terrible idea!)
            ("One", "drop"),
            ("Two", "drop"),
            ("Three", "drop"),
            ("Four", "drop"),
            ("Five", "drop"),
        ]);
    }
}

mod tween_panic_inwards {
    use super::{OneTween, TwoTween, ThreeTween, FourTween, FiveTween};
    use tween::stubs::{Request};
    use std::panic;

    tweens! {
        handler |_context: &mut Context| {
            super::log("main", "handler");
            super::response("main")
        };

        one: OneTween [ContextForOneTween];
        two: TwoTween [ContextForTwoTween];
        three: ThreeTween [ContextForThreeTween] = ThreeTween::new(::DesiredAction::PanicInward);
        four: FourTween [ContextForFourTween];
        five: FiveTween [ContextForFiveTween];
    }

    #[test]
    fn execute() {
        let tweens = Tweens::default();
        let _lock = super::lock();
        let response = panic::catch_unwind(|| {
            tweens.handle(Request::default())
        });
        assert!(response.is_err());
        assert_eq!(super::get_actions(), [
            ("OneTween", "inward"),
            ("One", "new"),
            ("TwoTween", "inward"),
            ("Two", "new"),
            ("ThreeTween", "inward"),
            ("ThreeTween", "inward panic"),
            // As before, note the order. It’s OK.
            ("One", "drop"),
            ("Two", "drop"),
        ]);
    }
}

mod tween_panic_outwards {
    use super::{OneTween, TwoTween, ThreeTween, FourTween, FiveTween};
    use tween::stubs::{Request};
    use std::panic;

    tweens! {
        handler |_context: &mut Context| {
            super::log("main", "handler");
            super::response("main")
        };

        one: OneTween [ContextForOneTween];
        two: TwoTween [ContextForTwoTween];
        three: ThreeTween [ContextForThreeTween] = ThreeTween::new(::DesiredAction::PanicOutward);
        four: FourTween [ContextForFourTween];
        five: FiveTween [ContextForFiveTween];
    }

    #[test]
    fn execute() {
        let tweens = Tweens::default();
        let _lock = super::lock();
        let response = panic::catch_unwind(|| {
            tweens.handle(Request::default())
        });
        assert!(response.is_err());
        assert_eq!(super::get_actions(), [
            ("OneTween", "inward"),
            ("One", "new"),
            ("TwoTween", "inward"),
            ("Two", "new"),
            ("ThreeTween", "inward"),
            ("Three", "new"),
            ("FourTween", "inward"),
            ("Four", "new"),
            ("FiveTween", "inward"),
            ("Five", "new"),
            ("main", "handler"),
            ("FiveTween", "outward"),
            ("Five", "drop"),
            ("FourTween", "outward"),
            ("Four", "drop"),
            ("ThreeTween", "outward"),
            ("ThreeTween", "outward panic"),
            ("Three", "drop"),
            ("Response", "drop"),
            // As before, note the order of the rest. It’s OK. (Three got dropped first because it
            // had already been moved out of the context.)
            ("One", "drop"),
            ("Two", "drop"),
        ]);
    }
}
